//
//  NET.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 28/01/23.
//

import SwiftyJSON
import Alamofire

class NET {
    
    class func simpleRequest(from url: URL, method: HTTPMethod, params: [String: Any]?, completion: @escaping (JSON?) -> Void) {
        if Reachability.isConnectedToNetwork() {
            AF.request(url, method: method, parameters: params, encoding: URLEncoding.default).response { (response) in
                switch response.result {
                case .success(_):
                    if let datas = response.data {
                        completion(JSON(datas))
                    }
                case .failure(_):
                    completion(nil)
                }
            }
        } else {
            print("no connection")
        }
    }
    
    
}
