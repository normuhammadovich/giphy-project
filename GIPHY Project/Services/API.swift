//
//  API.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 28/01/23.
//


import SwiftyJSON
import Alamofire

class API {
    
    //APIs
    
    static let basurl: String = "https://api.giphy.com/v1/gifs/"
    
    
    ///     get gifs
    class func getGifs(offset: Int, limit: Int, completion: @escaping ([GifDM]?) -> Void) {
        
        guard let url = URL(string: basurl + "trending") else { return }
        
        let params: [String: Any] = [
            "api_key"   :   Constants.api_key,
            "limit"     :   limit,
            "offset"    :   offset
        ]
        
        NET.simpleRequest(from: url, method: .get, params: params) { data in
            guard let data = data else { completion(nil); return }
                        
            if data["meta"]["status"].intValue == 200 {
                
                if let subData = data["data"].array {
                    let gifs = subData.compactMap{ GifDM(json: $0) }
                    completion(gifs)
                }
                
            }
            
        }
        
    }
    
    
    
    
    
}
