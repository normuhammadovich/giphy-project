//
//  HomeVC.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 27/01/23.
//

import UIKit
import Photos

class HomeVC: UIViewController {
 
//    prepare collectionView
    private lazy var mosaicCollectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: getLayout())
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(cellClass: HomeCVC.self)
        cv.backgroundColor = nil
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    lazy var viewModel = HomeVM()
    
    override func viewDidLoad() {
        setUpUI()
        dataBinding()
        viewModel.getGifs(offset: viewModel.offsetOfGifs, limit: viewModel.limitOfGifs)
    }
    
//    set up ui elements
    private func setUpUI() {
        view.backgroundColor = .black
        view.addSubview(mosaicCollectionView)
        [
            mosaicCollectionView.leadingAnchor.constraint(equalTo:
                                                            view.leadingAnchor),
            mosaicCollectionView.trailingAnchor.constraint(equalTo:
                                                            view.trailingAnchor),
            mosaicCollectionView.topAnchor.constraint(equalTo:
                                                        view.topAnchor),
            mosaicCollectionView.bottomAnchor.constraint(equalTo:
                                                            view.bottomAnchor),
        ].forEach({ $0.isActive = true })
    }
    
//    prepare Mosaic collectionViewLayout
    private func getLayout() -> MosaicCollectionViewLayout {
        let layout = MosaicCollectionViewLayout()
        layout.sectionInset = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        layout.minimumColumnSpacing = 4
        layout.minimumInteritemSpacing = 4
        return layout
    }

//    data biding for getting new gifs
    private func dataBinding(){
        self.viewModel.gifs.bind { _ in
            self.mosaicCollectionView.reloadData()
        }
    }
 
}


// MARK: - UICollectionViewDataSource
extension HomeVC: UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.gifs.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCVC.reuseID, for: indexPath) as! HomeCVC
        cell.updateCell(gif: viewModel.gifs.value[indexPath.row], index: indexPath.row)
        return cell
    }
    
}


// MARK: - MosaicCollectionViewLayoutDelegate
extension HomeVC: MosaicCollectionViewLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ShareVC()
        vc.viewModel.gif.value = self.viewModel.gifs.value[indexPath.item]
        self.present(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return viewModel.gifs.value[indexPath.item].setSize
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        pagination
        if (indexPath.row % viewModel.limitOfGifs == viewModel.limitOfGifs-2) &&  viewModel.lastOfGifs.value.count >= viewModel.limitOfGifs && indexPath.row >= viewModel.gifs.value.count - 2 {
            viewModel.getGifs(offset: viewModel.offsetOfGifs, limit: viewModel.limitOfGifs)
        }
    }
    
}

