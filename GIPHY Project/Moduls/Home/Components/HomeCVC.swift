//
//  HomeCVC.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 28/01/23.
//

import UIKit
import FLAnimatedImage
import SDWebImage

class HomeCVC: UICollectionViewCell {

//    prepare imageView for gif
    private lazy var imageView: FLAnimatedImageView = {
        let imageView = FLAnimatedImageView()
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 8
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return imageView
    }()
    
//    prepare skeletonView for animation while gif downloading
    private lazy var skeletonView: SkeletonView = {
        let view = SkeletonView()
        view.clipsToBounds = true
        view.layer.cornerRadius = 8
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
        self.autoresizesSubviews = true
        setUpUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func prepareForReuse() {
        super.prepareForReuse()

    }

//    update cell when cerating cell
    func updateCell(gif: GifDM, index: Int) {
        
        if gif.isGifDownloaded {
            self.skeletonView.stopAnimating()
        } else {
            self.skeletonView.startAnimating()
        }
        
        imageView.sd_setImage(with: gif.gifDownloadUrl, completed: { _, _, _, url in
            gif.isGifDownloaded = true
            self.skeletonView.stopAnimating()
        })
        
    }
    
    
//    set up ui elements
    func setUpUI() {
        
        imageView.frame = self.bounds
        skeletonView.frame = self.bounds
    
        self.addSubview(skeletonView)
        skeletonView.startAnimating()
        
        [
            skeletonView.leadingAnchor.constraint(equalTo:
                                                            self.leadingAnchor),
            skeletonView.trailingAnchor.constraint(equalTo:
                                                            self.trailingAnchor),
            skeletonView.topAnchor.constraint(equalTo:
                                                self.topAnchor),
            skeletonView.bottomAnchor.constraint(equalTo:
                                                            self.bottomAnchor),
        ].forEach({ $0.isActive = true })
        
        self.addSubview(imageView)
        
        [
            imageView.leadingAnchor.constraint(equalTo:
                                                self.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo:
                                                    self.trailingAnchor),
            imageView.topAnchor.constraint(equalTo:
                                            self.topAnchor),
            imageView.bottomAnchor.constraint(equalTo:
                                                self.bottomAnchor),
        ].forEach({ $0.isActive = true })
        
    }
    
}
