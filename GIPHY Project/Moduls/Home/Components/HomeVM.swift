//
//  HomeVM.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 27/01/23.
//

import Foundation

class HomeVM {
    
    var gifs: Box<[GifDM]> = Box([])
    var lastOfGifs: Box<[GifDM]> = Box([])
    var offsetOfGifs: Int = 0
    var limitOfGifs: Int = 20
    
    func getGifs(offset: Int, limit: Int) {
        API.getGifs(offset: offset, limit: limit) { g in
            guard let g = g else { return }
            self.gifs.value.append(contentsOf: g)
            self.lastOfGifs.value = g
            self.offsetOfGifs += g.count
        }
    }
   
}
