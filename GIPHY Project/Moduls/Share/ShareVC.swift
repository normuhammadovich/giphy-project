//
//  ShareVC.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 29/01/23.
//

import UIKit
import FLAnimatedImage
import MobileCoreServices
import Photos

class ShareVC: UIViewController {
    
//    prepare back button
    private lazy var backBtn: UIButton = {
       let b = UIButton()
        b.tintColor = .white
        b.setImage(UIImage(systemName: "xmark"), for: .normal)
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
//    prepare share button
    private lazy var shareBtn: UIButton = {
       let b = UIButton()
        b.tintColor = .white
        b.setImage(UIImage(systemName: "square.and.arrow.up"), for: .normal)
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
//    prepare stackView for back and share button
    private lazy var topBtnHStackV: UIStackView = {
       let s = UIStackView()
        s.axis = .horizontal
        s.alignment = .fill
        s.distribution = .equalSpacing
        s.spacing = 20
        s.translatesAutoresizingMaskIntoConstraints = false
        return s
    }()
    
//    prepare stackView for gif imageView and button's stackView
    private lazy var topVStackV: UIStackView = {
       let s = UIStackView()
        s.axis = .vertical
        s.alignment = .fill
        s.distribution = .fill
        s.spacing = 20
        s.translatesAutoresizingMaskIntoConstraints = false
        return s
    }()
    
//    prepare imageView for gif
    private lazy var gifImageView: FLAnimatedImageView = {
        let imgV = FLAnimatedImageView()
        imgV.clipsToBounds = true
        imgV.backgroundColor = .clear
        imgV.layer.cornerRadius = 8
        imgV.contentMode = .scaleAspectFit
        imgV.translatesAutoresizingMaskIntoConstraints = false
        imgV.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return imgV
    }()
    
//    prepare button for copy gif link
    private lazy var copyGifLinkBtn: UIButton = {
       let b = UIButton()
        b.backgroundColor = .gPurple
        b.setTitleColor(.white, for: .normal)
        b.setTitle("Copy Gif Link", for: .normal)
        b.titleLabel?.font = .systemFont(ofSize: 18, weight: .medium)
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
//    prepare button for copy gif
    private lazy var copyGifBtn: UIButton = {
       let b = UIButton()
        b.backgroundColor = .darkGray
        b.setTitleColor(.white, for: .normal)
        b.setTitle("Copy Gif", for: .normal)
        b.titleLabel?.font = .systemFont(ofSize: 18, weight: .medium)
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
//    prepare button for cancel
    private lazy var cancelBtn: UIButton = {
       let b = UIButton()
        b.backgroundColor = .black
        b.setTitleColor(.white, for: .normal)
        b.setTitle("Cancel", for: .normal)
        b.titleLabel?.font = .systemFont(ofSize: 18, weight: .medium)
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
//    prepare button for save gif
    private lazy var saveGifBtn: UIButton = {
        let b = UIButton()
        b.backgroundColor = .gPurple
        b.setTitleColor(.white, for: .normal)
        b.setTitle("Save Gif", for: .normal)
        b.titleLabel?.font = .systemFont(ofSize: 18, weight: .medium)
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
//    prepare stackView for cancel and copy buttons
    private lazy var bottomVStackV: UIStackView = {
       let s = UIStackView()
        s.axis = .vertical
        s.alignment = .fill
        s.distribution = .fillEqually
        s.spacing = 6
        s.translatesAutoresizingMaskIntoConstraints = false
        return s
    }()
    
//    prepare button for share gif with telegramm
    private lazy var shareTgBtn: UIButton = {
        let b = UIButton()
         b.tintColor = .white
         b.setImage(UIImage(named: "tg"), for: .normal)
         b.translatesAutoresizingMaskIntoConstraints = false
         return b
     }()
    
//    prepare button for share gif with imessage
    private lazy var shareIMessBtn: UIButton = {
        let b = UIButton()
         b.tintColor = .white
         b.setImage(UIImage(named: "ins"), for: .normal)
         b.translatesAutoresizingMaskIntoConstraints = false
         return b
     }()
    
//    prepare stackView for share buttons
    private lazy var shareHStackV: UIStackView = {
        let s = UIStackView()
        s.axis = .horizontal
        s.alignment = .leading
        s.distribution = .fillEqually
        s.spacing = 6
        s.translatesAutoresizingMaskIntoConstraints = false
        return s
    }()
    
    lazy var viewModel = ShareVM()
    
    override func viewDidLoad() {
   
        setUpUI()
        addActions()
        
    }
    
//    set up UI elements
    private func setUpUI() {
        
        bottomVStackV.isHidden = true
        shareHStackV.isHidden = true
        view.backgroundColor = .black
        
        topBtnHStackV.addArrangedSubview(backBtn)
        topBtnHStackV.addArrangedSubview(shareBtn)
        
        topVStackV.addArrangedSubview(topBtnHStackV)
        topVStackV.addArrangedSubview(gifImageView)
        
        shareHStackV.addArrangedSubview(shareTgBtn)
        shareHStackV.addArrangedSubview(shareIMessBtn)
        
        bottomVStackV.addArrangedSubview(copyGifLinkBtn)
        bottomVStackV.addArrangedSubview(saveGifBtn)
        bottomVStackV.addArrangedSubview(copyGifBtn)
        bottomVStackV.addArrangedSubview(cancelBtn)
        
        self.view.addSubview(bottomVStackV)
        self.view.addSubview(shareHStackV)
        self.view.addSubview(topVStackV)
        
        [
            shareTgBtn.heightAnchor.constraint(equalToConstant: 40),
            shareTgBtn.widthAnchor.constraint(equalToConstant: 40)
        ].forEach({ $0.isActive = true })
        
        [
            shareIMessBtn.heightAnchor.constraint(equalToConstant: 40),
            shareIMessBtn.widthAnchor.constraint(equalToConstant: 40)
        ].forEach({ $0.isActive = true })
        
        [
            backBtn.heightAnchor.constraint(equalToConstant: 50),
            backBtn.widthAnchor.constraint(equalToConstant: 50)
        ].forEach({ $0.isActive = true })
        
        [
            shareBtn.heightAnchor.constraint(equalToConstant: 50),
            shareBtn.widthAnchor.constraint(equalToConstant: 50)
        ].forEach({ $0.isActive = true })
        
        [
            gifImageView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width-40),
            gifImageView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width-40)
        ].forEach({ $0.isActive = true })
        
        copyGifLinkBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        [
            topVStackV.leadingAnchor.constraint(equalTo:
                                                            view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            topVStackV.trailingAnchor.constraint(equalTo:
                                                            view.safeAreaLayoutGuide.trailingAnchor ,constant: -20),
            topVStackV.topAnchor.constraint(equalTo:
                                                        view.safeAreaLayoutGuide.topAnchor, constant: 0),
        ].forEach({ $0.isActive = true })
        
        [
            bottomVStackV.leadingAnchor.constraint(equalTo:
                                                            view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            bottomVStackV.trailingAnchor.constraint(equalTo:
                                                            view.safeAreaLayoutGuide.trailingAnchor ,constant: -20),
            bottomVStackV.bottomAnchor.constraint(equalTo:
                                                        view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
        ].forEach({ $0.isActive = true })
        
        [
            shareHStackV.leadingAnchor.constraint(equalTo:
                                                    view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            shareHStackV.bottomAnchor.constraint(equalTo:
                                                    bottomVStackV.topAnchor, constant: -10),
        ].forEach({ $0.isActive = true })
        
        gifImageView.sd_setImage(with: viewModel.gif.value?.gifOriginalDownloadUrl)
        
    }
    
//     add actions for burrons
    private func addActions() {
        backBtn.addTarget(self, action: #selector(backTapped), for: .touchUpInside)
        shareBtn.addTarget(self, action: #selector(shareTapped), for: .touchUpInside)
        copyGifLinkBtn.addTarget(self, action: #selector(copyGifLinkTapped), for: .touchUpInside)
        copyGifBtn.addTarget(self, action: #selector(copyGifTapped), for: .touchUpInside)
        cancelBtn.addTarget(self, action: #selector(cancelTapped), for: .touchUpInside)
        saveGifBtn.addTarget(self, action: #selector(saveGifTapped), for: .touchUpInside)
        shareTgBtn.addTarget(self, action: #selector(shareTgTapped), for: .touchUpInside)
        shareIMessBtn.addTarget(self, action: #selector(shareInstagram), for: .touchUpInside)
    }
    
//  @objc functions for buttons
    @objc private func backTapped() {
        self.dismiss(animated: true)
    }

    @objc private func shareTapped() {
        bottomVStackV.isHidden = false
        shareHStackV.isHidden = false
    }
    
    @objc private func copyGifLinkTapped() {
        UIPasteboard.general.url = viewModel.gif.value?.gifDownloadUrl
        showSuccessAlert(title: "👍🏻", message: "Copied to Clipboard", completion: nil)
        bottomVStackV.isHidden = true
        shareHStackV.isHidden = true
    }

    @objc private func copyGifTapped() {
        showSuccessAlert(title: "👍🏻", message: "Copied to Clipboard", completion: nil)
        bottomVStackV.isHidden = true
        shareHStackV.isHidden = true
        let imgD = gifImageView.animatedImage.data
        copyFileToPasteboard(data: imgD!)
        
    }
    
    @objc private func cancelTapped() {
        bottomVStackV.isHidden = true
        shareHStackV.isHidden = true
    }
    
    @objc private func saveGifTapped() {
        saveGifIntoPhotos()
        bottomVStackV.isHidden = true
        shareHStackV.isHidden = true
    }
    
    @objc private func shareTgTapped() {
        bottomVStackV.isHidden = true
        shareHStackV.isHidden = true
        openTg()
    }
    
    @objc private func shareIMTapped() {
        bottomVStackV.isHidden = true
        shareHStackV.isHidden = true
    }
    
//     copy gif function
    private func copyFileToPasteboard(data: Data){
        let pasteboard = UIPasteboard.general
        pasteboard.items = []
        pasteboard.setData(data, forPasteboardType: kUTTypeGIF as String)
    }
    
//    save git to photos function
    private func saveGifIntoPhotos() {
        PHPhotoLibrary.shared().performChanges({
            PHAssetCreationRequest.forAsset().addResource(with: .photo, data: self.gifImageView.animatedImage.data, options: nil)
            DispatchQueue.main.async {
                self.showSuccessAlert(title: "👍🏻", message: "Saved into Photos", completion: nil)
            }
        })
    }
    
    private func openTg() {
        let screenName =  "Asliddin_Mansurovich"
        let appURL = NSURL(string: "tg://resolve?domain=\(screenName)")!
        let webURL = NSURL(string: "https://t.me/\(screenName)")!
        if UIApplication.shared.canOpenURL(appURL as URL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL as URL, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(appURL as URL)
            }
        }
        else {
            //redirect to safari because user doesn't have Telegram
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(webURL as URL, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(webURL as URL)
            }
        }
    }
    
    @objc private func shareInstagram() {

        let Username =  "asliddin.mansurovich" // Your Instagram Username here
        let appURL = URL(string: "instagram://user?username=\(Username)")!
        let application = UIApplication.shared

        if application.canOpenURL(appURL) {
            application.open(appURL)
        } else {
            // if Instagram app is not installed, open URL inside Safari
            let webURL = URL(string: "https://instagram.com/\(Username)")!
            application.open(webURL)
        }

    }
    
    
    
    
}


