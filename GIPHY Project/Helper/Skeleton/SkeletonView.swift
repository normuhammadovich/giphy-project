//
//  SkeletonDisplayable.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 28/01/23.
//

import UIKit


class SkeletonView: UIView {
    
    private var startLocations : [NSNumber] = [-1.0,-0.5, 0.0]
    private var endLocations : [NSNumber] = [1.0,1.5, 2.0]
    
    private var gradientBackgroundColorP : CGColor = UIColor(red: 0.376, green: 0.345, blue: 0.965, alpha: 1).cgColor
    private var gradientMovingColorP : CGColor = UIColor(red: 0.346, green: 0.315, blue: 0.915, alpha: 1).cgColor
    
    private var gradientBackgroundColorR : CGColor = UIColor(red: 0.831, green: 0.329, blue: 0.702, alpha: 1).cgColor
    private var gradientMovingColorR : CGColor = UIColor(red: 0.781, green: 0.279, blue: 0.652, alpha: 1).cgColor
    
    private var gradientBackgroundColorG : CGColor = UIColor(red: 0.424, green: 0.898, blue: 0.820, alpha: 1).cgColor
    private var gradientMovingColorG : CGColor = UIColor(red: 0.424, green: 0.898, blue: 0.820, alpha: 1).cgColor
    
    
    private var movingAnimationDuration : CFTimeInterval = 0.8
    private var delayBetweenAnimationLoops : CFTimeInterval = 0.4
    
//    var

    lazy var gradientLayer : CAGradientLayer = {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        gradientLayer.colors = [
            [
                gradientBackgroundColorP,
                gradientMovingColorP,
                gradientBackgroundColorP
            ],
            [
                gradientBackgroundColorR,
                gradientMovingColorR,
                gradientBackgroundColorR
            ],
            [
                gradientBackgroundColorG,
                gradientMovingColorG,
                gradientBackgroundColorG
            ]
        ].randomElement()
        gradientLayer.locations = self.startLocations
        self.layer.addSublayer(gradientLayer)
        return gradientLayer
    }()
    
    
    func startAnimating(){
        self.isHidden = false
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = self.startLocations
        animation.toValue = self.endLocations
        animation.duration = self.movingAnimationDuration
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        
        
        let animationGroup = CAAnimationGroup()
        animationGroup.duration = self.movingAnimationDuration + self.delayBetweenAnimationLoops
        animationGroup.animations = [animation]
        animationGroup.repeatCount = .infinity
        self.gradientLayer.add(animationGroup, forKey: animation.keyPath)
    }
    
    func stopAnimating() {
        self.isHidden = true
        self.gradientLayer.removeAllAnimations()
//        self.layer.removeAllAnimations()
    }
    
}


