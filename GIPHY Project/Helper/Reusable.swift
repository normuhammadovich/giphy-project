//
//  Reusable.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 27/01/23.
//

import UIKit

protocol Reusable {
    
    static var reuseID: String { get }
    
}


extension Reusable {
    
    static var reuseID: String {
        return String(describing: self)
    }
    
}
