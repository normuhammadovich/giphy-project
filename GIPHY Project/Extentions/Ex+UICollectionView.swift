//
//  Ex+UICollectionView.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 27/01/23.
//

import UIKit

extension UICollectionView {
    
    func register<T: UICollectionViewCell>(cellClass: T.Type = T.self) {
        
        let bundle = Bundle(for: cellClass.self)
        
        if bundle.path(forResource: cellClass.reuseID, ofType: "nib") != nil {
            let nib = UINib(nibName: cellClass.reuseID, bundle: bundle)
            register(nib, forCellWithReuseIdentifier: cellClass.reuseID)
        } else {
            register(cellClass.self, forCellWithReuseIdentifier: cellClass.reuseID)
        }
        
    }
    
}
