//
//  Ex+UIViewController.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 27/01/23.
//

import UIKit

 extension UIViewController {
    
    static func nib() -> Self {
        
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: Bundle.init(for: Self.self))
        }
        return instantiateFromNib()
        
    }
     
     func showSuccessAlert(title: String?, message: String?, completion: (() -> Void)? = nil) {
         
         let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
         let action = UIAlertAction(title: "OK", style: .default) { (action) in
             if let _ = completion {
                 completion!()
             }
         }
         alert.addAction(action)
         present(alert, animated: true, completion: nil)
         
     }
     
 }
