//
//  Ex+UIColor.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 28/01/23.
//

import Foundation
import UIKit

extension UIColor {
    
    static let gPurple = UIColor(named: "purple") ?? .purple
    static let gLightRed = UIColor(named: "lightRed") ?? .red
    static let gLightGreen = UIColor(named: "lightGreen") ?? .green
    
}
