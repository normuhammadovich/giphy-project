//
//  GifDM.swift
//  GIPHY Project
//
//  Created by Asliddin Mahmudov on 28/01/23.
//

import UIKit
import SwiftyJSON

class GifDM {
    
    var type: String
    var id: String
    var height: Double
    var width: Double
    var urlStr: String
    var originalStr : String
    var onloading: String
    var backColor: UIColor = .gPurple
    var isGifDownloaded: Bool = false
    
    var setSize: CGSize {
        let w = (Constants.screenSize.width-4)/2
        let h = w*aspectRatio
        return CGSize(width: w, height: h)
    }
    
    var aspectRatio: Double {
        return height/width
    }
    
    var gifDownloadUrl: URL? {
        return URL(string: urlStr)
    }
    
    var gifOriginalDownloadUrl: URL? {
        return URL(string: originalStr)
    }
    
    var loadingUrl: URL? {
        return URL(string: onloading)
    }
    
    init(json: JSON) {
        
        self.type   =   json["type"].stringValue
        self.id     =   json["id"].stringValue
        self.height =   json["images"]["preview_gif"]["height"].doubleValue
        self.width  =   json["images"]["preview_gif"]["width"].doubleValue
        self.urlStr =   json["images"]["preview_gif"]["url"].stringValue
        self.onloading = json["analytics"]["onload"]["url"].stringValue
        self.originalStr =   json["images"]["original"]["url"].stringValue
        self.backColor = [UIColor.gPurple,UIColor.gLightRed,UIColor.gLightGreen].randomElement() ?? .gPurple
        
    }
    
}
